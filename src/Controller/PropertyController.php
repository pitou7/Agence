<?php

namespace App\Controller;

use App\Entity\Property;
use App\Repository\PropertyRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PropertyController extends AbstractController
{

    /**
     * @var PropertyRepository
     */
    private $repository;
    public function __construct(PropertyRepository $repository)
    {
        $this->repository = $repository;
    }
    /**
     * @Route("/biens", name="details.index")
     */
    public function index(): Response
    {
       /* $property = new Property();
        $property->setTitre('Biens N 1')
            ->setPrix(15000)
            ->setPieces(2)
            ->setChambre(1)
            ->setDescription("Chambre moderne situe au niveau de pk16")
            ->setEtage(1)
            ->setVille("Douala-Pk16")
            ->setSurface(30)
            ->setAdresse('Pk16');
        $em = $this->getDoctrine()->getManager();
        $em->persist($property);
        $em->flush();*/ 
        return $this->render('property/index.html.twig', [
            'current_menu' => 'properties'
        ]);
    }

    /**
     * @Route("/biens/{slug}-{id}", name="property.show", requirements={"slug": "[a-z0-9\-]*"})
     */
    public function show($slug, $id): Response
    {
        $property = $this->repository->find($id);
        return $this->render('property/show.html.twig', [
            'property' => $property,
            'current_menu' => 'properties'
        ]);
    }
}